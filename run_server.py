# -*- coding: utf-8 -*-
import os
import sys
from july.util import reset_option, parse_config_file
from july.app import JulyApplication
from july.web import init_options, run_server, JulyHandler
import motor
import tornado
from tornado.options import options
from app.util.july import JinjaLoader, NewJulyApplication
import datetime
PROJDIR = os.path.abspath(os.path.dirname(__file__))
ROOTDIR = os.path.split(PROJDIR)[0]
TEMPLATES_DIR = PROJDIR + '/templates'
try:
    import app
    print('Start app version: %s' % app.__version__)
    #todo 怎么是ROOTDIR
    sys.path.append(ROOTDIR)
except ImportError:
    import site
    site.addsitedir(ROOTDIR)
    print('Development of app')

reset_option('template_path', os.path.join(PROJDIR, "templates"))
reset_option('login_url', 'account/signin', type=str)
reset_option('static_path', os.path.join(PROJDIR, 'static'))
reset_option('static_url_prefix', '/static/', type=str)
# reset_option('locale_path', os.path.join(PROJDIR, 'locale'))
#todo ?
reset_option('sqlalchemy_kwargs', {}, type=dict)

def create_application():

    try:
        client = motor.MotorClient('%s:%s'%(options.mongo_host, options.mongo_port))
        mongo_db = client[options.mongo_db]
    except:
        print "cannot connect mongodb, check the config.yaml"
        sys.exit(0)

    settings = dict(
        debug=options.debug,
        autoescape=options.autoescape,
        cookie_secret=options.cookie_secret,
        cookie_expires=31,
        xsrf_cookies=False,
        login_url=options.login_url,
        template_path=options.template_path,
        july_template_path=options.template_path,
        static_path=options.static_path,
        static_url_prefix=options.static_url_prefix,
        mongo_db=mongo_db,
        template_loader=JinjaLoader(TEMPLATES_DIR)
    )
    #: init application
    application = NewJulyApplication(**settings)

    application.register_app('app.learn.handlers.core.learn', url_prefix='/learn')

    return application

if __name__ == '__main__':
    init_options()
    parse_config_file(os.path.join(PROJDIR, 'etc/server.conf'))
    tornado.options.parse_command_line()
    application = create_application()

    run_server(application)