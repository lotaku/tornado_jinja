# -*- coding: utf-8 -*-
from july.app import JulyApplication
from july.template import JulyLoader
import threading
from tornado import template, web
from tornado.wsgi import WSGIApplication
from tornado.web import Application, URLSpec
import jinja2

class TTemplate(object):
    def __init__(self, template_instance):
        self.template_instance = template_instance

    def generate(self, **kwargs):
        return self.template_instance.render(**kwargs)


class JinjaLoader(template.BaseLoader):

    def __init__(self, root_directory, *args, **kwargs):
        super(JinjaLoader, self).__init__(**kwargs)
        if isinstance(root_directory, str):
            self.roots = [root_directory]
        else:
            assert isinstance(root_directory, (list, tuple)), "roots should be lists"
            self.roots = root_directory
        self.jinja_env = \
        jinja2.Environment(loader=jinja2.FileSystemLoader(root_directory), **kwargs)
        self.templates = {}
        self.lock = threading.RLock()

    def resolve_path(self, name, parent_path=None):
        return name

    def _create_template(self, name):
        template_instance = TTemplate(self.jinja_env.get_template(name))
        print 'Template "%s" --> Use Jinja2 Template Engine' % name
        return template_instance

class NewJulyApplication(JulyApplication):

    def __call__(self):
        kwargs = {}
        if 'autoescape' in self.settings:
            kwargs['autoescape'] = self.settings['autoescape']
        path = self.settings.pop('template_path')

        if self.settings['template_loader']:
            print 'App Use Template loader: %s ' % self.settings['template_loader']
        else:
            loader = JulyLoader(path, **kwargs)
            self.settings['template_loader'] = loader

        # loader = JulyLoader(path, **kwargs)
        # self.settings['template_loader'] = loader

        if self.wsgi:
            app = WSGIApplication(self.handlers, self.default_host,
                                  **self.settings)
            return app
        app = Application(
            self.handlers, self.default_host, self.transforms,
            **self.settings
            # self.wsgi, **self.settings
        )
        return app