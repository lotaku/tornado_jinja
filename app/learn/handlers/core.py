# -*- coding: utf-8 -*-
from july.app import JulyApp
from app.learn.handlers import LearnJulyHandlers
from tornado.web import asynchronous
import os


templates_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..'))

class IndexHandler(LearnJulyHandlers):

    # @asynchronous
    def get(self, *args, **kwargs):
        context = {'name': 'zz'}
        self.render('learn/index.html', person=context)

handlers = [
    ('/', IndexHandler),
    ('', IndexHandler),
]

learn = JulyApp('learn', __name__, handlers=handlers)